#!/bin/bash

sudo cp ./testbug.service /etc/systemd/system/

sudo systemctl daemon-reload

sudo systemctl start testbug.service

systemctl status testbug.service -l --no-pager

journalctl -n5 --no-hostname -u testbug.service

# Cleanup
sudo rm /etc/systemd/system/testbug.service

