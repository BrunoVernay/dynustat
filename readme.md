
# Bug illustration

There are 2 bugs in systemd and SELinux policy:
- [systemd services with DynamicUser=yes fail: avc: denied](https://bugzilla.redhat.com/show_bug.cgi?id=1559281)
- [Failed to set up special execution directory in /var/lib: File exists](https://bugzilla.redhat.com/show_bug.cgi?id=1559286)

This repo allows to test very simply.


